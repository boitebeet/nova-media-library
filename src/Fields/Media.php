<?php

namespace BoiteBeet\AdvancedNovaMediaLibrary\Fields;

use App\Draft;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Laravel\Nova\Fields\Field;
use Laravel\Nova\Http\Requests\NovaRequest;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Media extends Field
{
    use HandlesCustomPropertiesTrait, HandlesConversionsTrait, HandlesExistingMediaTrait;

    public $component = 'advanced-media-library-field';

    protected $setFileNameCallback;
    protected $setNameCallback;
    protected $serializeMediaCallback;
    protected $responsive = false;

    protected $collectionMediaRules = [];
    protected $singleMediaRules = [];

    protected $customHeaders = [];

    protected $defaultValidatorRules = [];

    protected $forceMultiple = null;

    public $meta = ['type' => 'media'];

    public function serializeMediaUsing(callable $serializeMediaUsing): self
    {
        $this->serializeMediaCallback = $serializeMediaUsing;

        return $this;
    }

    public function fullSize(): self
    {
        return $this->withMeta(['fullSize' => true]);
    }

    /**
     * Apply filters for the gallery of existing medias
     * @param array $filters
     * @return mixed
     */
    public function existingFilters(array $filters)
    {
        $metaFilters = isset($this->meta()['filters']) ? $this->meta()['filters'] : [];
        foreach ($filters as $filter) {
            array_push($metaFilters, $filter);
        }
        return $this->withMeta(['filters' => $metaFilters]);
    }

    public function rules($rules): self
    {
        $this->collectionMediaRules = ($rules instanceof Rule || is_string($rules)) ? func_get_args() : $rules;

        return $this;
    }

    public function singleMediaRules($rules): self
    {
        $this->singleMediaRules = ($rules instanceof Rule || is_string($rules)) ? func_get_args() : $rules;

        return $this;
    }

    public function customHeaders(array $headers): self
    {
        $this->customHeaders = $headers;

        return $this;
    }

    /**
     * Set the responsive mode, which enables the creation of responsive images on upload
     *
     * @param boolean $responsive
     *
     * @return $this
     */
    public function withResponsiveImages($responsive = true)
    {
        $this->responsive = $responsive;

        return $this;
    }

    /**
     * Set a filename callable callback
     *
     * @param callable $callback
     *
     * @return $this
     */
    public function setFileName($callback)
    {
        $this->setFileNameCallback = $callback;

        return $this;
    }

    /**
     * Set a name callable callback
     *
     * @param callable $callback
     *
     * @return $this
     */
    public function setName($callback)
    {
        $this->setNameCallback = $callback;

        return $this;
    }

    /**
     * Set the maximum accepted file size for the frontend in kBs
     *
     * @param int $maxSize
     *
     * @return $this
     */
    public function setMaxFileSize(int $maxSize)
    {
        return $this->withMeta(['maxFileSize' => $maxSize]);
    }

    /**
     * Validate the file's type on the frontend side
     * Example values for the array: 'image', 'video', 'image/jpeg'
     *
     * @param array $types
     *
     * @return $this
     */
    public function setAllowedFileTypes(array $types)
    {
        return $this->withMeta(['allowedFileTypes' => $types]);
    }

    /**
     * @param HasMedia $model
     * @param mixed $requestAttribute
     * @param mixed $attribute
     */
    public function fillAttributeFromRequest(NovaRequest $request, $requestAttribute, $model, $attribute)
    {
        $attr = $request['__media__'] ?? [];
        $data = $attr[$requestAttribute] ?? [];

        if ($attribute === 'ComputedField') {
            $attribute = call_user_func($this->computedCallback, $model);
        }

        collect($data)
            ->filter(function ($value) {
                return $value instanceof UploadedFile;
            })
            ->each(function ($media) use ($request, $requestAttribute) {
                $requestToValidateSingleMedia = $request->toArray();
                Arr::set($requestToValidateSingleMedia, $requestAttribute, $media);

                Validator::make($requestToValidateSingleMedia, [
                    $requestAttribute => array_merge($this->defaultValidatorRules, (array)$this->singleMediaRules),
                ])->validate();
            });

        $requestToValidateCollectionMedia = array_merge($request->toArray(), [
            $requestAttribute => $data,
        ]);

        Validator::make($requestToValidateCollectionMedia, [$requestAttribute => $this->collectionMediaRules])
                 ->validate();

        return function () use ($request, $data, $attribute, $model) {
            $this->handleMedia($request, $model, $attribute, $data);

            // fill custom properties for existing media
            $this->fillCustomPropertiesFromRequest($request, $model, $attribute);
        };
    }

    protected function handleMedia(NovaRequest $request, $model, $attribute, $data)
    {
        $remainingIds = $this->removeDeletedMedia($data, $model->getMedia($attribute));
        $newIds = $this->addNewMedia($request, $data, $model, $attribute);
        $existingIds = $this->addExistingMedia($request, $data, $model, $attribute, $model->getMedia($attribute));
        $this->setOrder($remainingIds->union($newIds)->union($existingIds)->sortKeys()->all());
    }

    private function setOrder($ids)
    {
        $mediaClass = config('media-library.media_model');
        $mediaClass::setNewOrder($ids);
    }

    private function addNewMedia(NovaRequest $request, $data, HasMedia $model, string $collection): Collection
    {
        return collect($data)
            ->filter(function ($value) {
                return $value instanceof UploadedFile;
            })->map(function (UploadedFile $file, int $index) use ($request, $model, $collection) {
                $media = $model->addMedia($file)->withCustomProperties($this->customProperties);

                if ($this->responsive) {
                    $media->withResponsiveImages();
                }

                if (!empty($this->customHeaders)) {
                    $media->addCustomHeaders($this->customHeaders);
                }

                if (is_callable($this->setFileNameCallback)) {
                    $media->setFileName(
                        call_user_func($this->setFileNameCallback, $file->getClientOriginalName(), $file->getClientOriginalExtension(), $model)
                    );
                } else {
                    $media->setFileName($this->defaultFilename($file));
                }

                if (is_callable($this->setNameCallback)) {
                    $media->setName(
                        call_user_func($this->setNameCallback, $file->getClientOriginalName(), $model)
                    );
                }

                $media = $media->toMediaCollection($collection);

                // fill custom properties for recently created media
                $this->fillMediaCustomPropertiesFromRequest($request, $media, $index, $collection);

                return $media->getKey();
            });
    }

    private function defaultFilename(UploadedFile $file) : string
    {
        $ext = $file->getClientOriginalExtension();
        $filename = str_replace(".{$ext}", '', $file->getClientOriginalName());
        return Str::slug($filename) . ".{$ext}";
    }

    private function removeDeletedMedia($data, Collection $medias): Collection
    {
        $remainingIds = collect($data)->filter(function ($value) {
            return !$value instanceof UploadedFile;
        })->map(function ($value) {
            return $value;
        });

        $medias->pluck('id')->diff($remainingIds)->each(function ($id) use ($medias) {
            /** @var Media $media */
            if ($media = $medias->where('id', $id)->first()) {
                $media->delete();
            }
        });

        return $remainingIds->intersect($medias->pluck('id'));
    }

    /**
     * Resolve the field's value for display.
     *
     * @param mixed $resource
     * @param string|null $attribute
     * @return void
     */
    public function resolveForDisplay($resource, $attribute = null, $group = null)
    {
        $this->resource = $resource;

        $attribute = $attribute ?? $this->attribute;

        if (!$this->displayCallback) {
            $this->resolve($resource, $attribute, $group);
        } elseif (is_callable($this->displayCallback)) {
            if ($attribute === 'ComputedField') {
                $this->value = call_user_func($this->computedCallback, $resource);
            }

            tap($this->value ?? $this->resolveAttribute($resource, $attribute), function ($value) use ($resource, $attribute, $group) {
                $this->value = call_user_func($this->displayCallback, $value, $resource, $attribute, $group);
            });
        }
    }

    /**
     * @param HasMedia|HasMediaTrait $resource
     * @param null $attribute
     */
    public function resolve($resource, $attribute = null, $group = null)
    {
        $collectionName = $attribute ?? $this->attribute;

        if ($collectionName === 'ComputedField') {
            $collectionName = call_user_func($this->computedCallback, $resource);
        }

        $this->value = $resource->getMedia($collectionName)
                                ->filter(function (\Spatie\MediaLibrary\MediaCollections\Models\Media $media) use ($group) {
                                    return $group ? $media->getCustomProperty('group') === $group : true;
                                })
                                ->map(function (\Spatie\MediaLibrary\MediaCollections\Models\Media $media) {
                                    return array_merge($this->serializeMedia($media), ['__media_urls__' => $this->getConversionUrls($media)]);
                                })->values();

        if ($collectionName) {
            $this->checkCollectionIsMultiple($resource, $collectionName);
        }
    }

    /**
     * @param HasMedia|HasMediaTrait $resource
     */
    protected function checkCollectionIsMultiple(HasMedia $resource, string $collectionName)
    {
        $resource->registerMediaCollections();

        $isMultiple = $this->forceMultiple ?? collect($resource->mediaCollections)
                ->where('name', $collectionName)
                ->first()
                ->singleFile ?? false;

        $this->withMeta(['multiple' => $isMultiple]);
    }

    public function serializeMedia(\Spatie\MediaLibrary\MediaCollections\Models\Media $media): array
    {
        if ($this->serializeMediaCallback) {
            return call_user_func($this->serializeMediaCallback, $media);
        }

        return $media->toArray();
    }

    public function multiple(): self
    {
        $this->withMeta(['multiple' => true]);
        $this->forceMultiple = true;

        return $this;
    }

    public function single(): self
    {
        $this->withMeta(['multiple' => false]);
        $this->forceMultiple = false;

        return $this;
    }

    /**
     * @deprecated
     * @see conversionOnIndexView
     */
    public function thumbnail(string $conversionOnIndexView): self
    {
        return $this->withMeta(compact('conversionOnIndexView'));
    }

    /**
     * @deprecated
     * @see conversionOnPreview
     */
    public function conversion(string $conversionOnPreview): self
    {
        return $this->withMeta(compact('conversionOnPreview'));
    }

    /**
     * @deprecated
     * @see conversionOnDetailView
     */
    public function conversionOnView(string $conversionOnDetailView): self
    {
        return $this->withMeta(compact('conversionOnDetailView'));
    }
}
