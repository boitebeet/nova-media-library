<?php

namespace BoiteBeet\AdvancedNovaMediaLibrary\Fields;

/**
 * @mixin Media
 */
trait HandlesConversionsTrait
{
    public function usingTemporaryUrls($value = 60)
    {
        return $this->withMeta([
            'usingTemporaryUrls' => true,
            'temporaryUrlTTL'    => $value
        ]);
    }

    public function conversionOnIndexView(string $conversionOnIndexView): self
    {
        return $this->withMeta(compact('conversionOnIndexView'));
    }

    public function conversionOnDetailView(string $conversionOnDetailView): self
    {
        return $this->withMeta(compact('conversionOnDetailView'));
    }

    public function conversionOnForm(string $conversionOnForm): self
    {
        return $this->withMeta(compact('conversionOnForm'));
    }

    public function conversionOnPreview(string $conversionOnPreview): self
    {
        return $this->withMeta(compact('conversionOnPreview'));
    }

    public function getDefaultConversionForCollection(string $collection, $view = null)
    {
        $defaultConversions = config("nova-media-library.collections-default-conversions.$collection");
        if (is_string($defaultConversions)) {
            return $defaultConversions;
        } elseif (is_array($defaultConversions)) {
            return $defaultConversions[$view] ?? ($defaultConversions['fallback'] ?? '');
        }
        return '';
    }

    private function setDefaultConversion(string $collection)
    {
        $defaults = [
            'conversionOnIndexView'  => $this->meta['conversionOnIndexView'] ?? $this->getDefaultConversionForCollection($collection, 'index') ?? '',
            'conversionOnDetailView' => $this->meta['conversionOnDetailView'] ?? $this->getDefaultConversionForCollection($collection, 'detail') ?? '',
            'conversionOnForm'       => $this->meta['conversionOnForm'] ?? $this->getDefaultConversionForCollection($collection, 'form') ?? '',
            'conversionOnPreview'    => $this->meta['conversionOnPreview'] ?? $this->getDefaultConversionForCollection($collection, 'preview') ?? '',
        ];
        if (method_exists($this, 'withMeta')) {
            $this->withMeta($defaults);
        } else {
            $this->meta = array_merge($this->meta ?? [], $defaults);
        }
    }

    public function getConversionUrls(\Spatie\MediaLibrary\MediaCollections\Models\Media $media): array
    {
        $this->setDefaultConversion($media->collection_name);
        return [
            // original needed several purposes like cropping
            '__original__' => $media->getFullUrl(),
            'indexView'    => $this->getConversionUrl($media, 'indexView'),
            'detailView'   => $this->getConversionUrl($media, 'detailView'),
            'form'         => $this->getConversionUrl($media, 'form'),
            'preview'      => $this->getConversionUrl($media, 'preview'),
        ];
    }

    public function getConversionUrl(\Spatie\MediaLibrary\MediaCollections\Models\Media $media, $view)
    {
        $meta = $this->meta['conversionOn' . ucfirst($view)] ?: '';
        $usingTemporaryUrls = !!($this->meta['usingTemporaryUrls'] ?? false);
        $temporaryUrlTTL = $usingTemporaryUrls ? ($this->meta['temporaryUrlTTL'] ?: 60) : null;
        if ($media->hasGeneratedConversion($meta)) {
            return $usingTemporaryUrls ? $media->getTemporaryUrl(now()->addMinutes($temporaryUrlTTL), $meta) : $media->getFullUrl($meta);
        }
        return $usingTemporaryUrls ? $media->getTemporaryUrl(now()->addMinutes($temporaryUrlTTL)) : $media->getFullUrl();
    }
}
