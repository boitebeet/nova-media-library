<?php

namespace BoiteBeet\AdvancedNovaMediaLibrary;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;

class AdvancedNovaMediaLibraryServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/nova-media-library.php' => config_path('nova-media-library.php'),
        ], 'nova-media-library');

        $this->publishTranslations();

        $this->app->booted(function () {
            $this->routes();
        });

        Nova::serving(function (ServingNova $event) {
            Nova::script('media-lib-images-field', __DIR__ . '/../dist/js/field.js');
            $this->loadTranslations();
        });
    }

    protected function routes()
    {
        if ($this->app->routesAreCached()) {
            return;
        }

        Route::middleware(['nova'])
             ->prefix('nova-vendor/ebess/advanced-nova-media-library')
             ->group(__DIR__ . '/../routes/api.php');
    }

    public function publishTranslations()
    {
        $this->publishes([
            __DIR__ . '/../resources/lang' => resource_path('lang/vendor/advanced-nova-media-library'),
        ], 'advanced-nova-media-library-lang');
    }

    public function loadTranslations()
    {
        if (file_exists(resource_path('lang/vendor/advanced-nova-media-library/' . app()->getLocale() . '.json'))) {
            Nova::translations(
                resource_path('lang/vendor/advanced-nova-media-library/' . app()->getLocale() . '.json')
            );
        } elseif (file_exists(__DIR__ . '/../resources/lang/' . app()->getLocale() . '.json')) {
            Nova::translations(
                __DIR__ . '/../resources/lang/' . app()->getLocale() . '.json'
            );
        }
    }
}
